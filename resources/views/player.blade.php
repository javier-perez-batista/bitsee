@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-10" style="margin-top: 1rem;">
            <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-centered"
                   controls preload="auto" height="720" width="1280">
                <source src="{{asset("/storage/images/videos/$video->name.mp4")}}" type="{{$mime}}" />
            </video>
            <div class="video_footer">
                <span class="">{{$video->name}}</span>
            </div><div class="video_footer">
                <a class="streaming_footer" href="/public/user/{{$video->channel->user->id}}">{{$video->channel->user->username}}  </a><span> playing </span><a class="streaming_footer" href="/public/juegos/{{$video->juego->id}}">{{$video->juego->name}}</a><span> at </span><a class="streaming_footer" href="/public/channel/{{$video->channel->id}}">  {{$video->channel->name}}</a>
            </div>
        </div>
        <div class="video_sugerencias col-lg-2">
            <div class="span_canales sugerencias_titulo" style="margin:0px">
                <span>Suggestions</span>
            </div>
            @include('videoList')
        </div>
    </div>
@endsection