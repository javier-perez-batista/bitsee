@extends('layouts.master')

@section('content')
    <div class="row explorar_selector">
        <select class="explorar_select" onchange="getExplorarElements()" name="elementos_explorar" id="elementos_explorar">
            <option selected value="0">Games</option>
            <option value="1">Channels</option>
            <option value="2">Users</option>
        </select>

    </div>
    <div id="explora_list">
        @include('juegos.juegosList')
    </div>
@endsection