<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'name' => $faker->name,
        'profile_picture'=> $faker->image('public/storage/images/',140 ,140, null, false),
        'banner_image'=> $faker->image('public/storage/images/',1300 ,380, null, false),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'age' => rand(18,45),
        'favs_games' => rand(1,50).','.rand(1,50).','.rand(1,50),
        'twitter' => $faker->userName,
        'facebook' => $faker->userName,
        'youtube' => $faker->userName,
        'instagram' => $faker->userName,
    ];
});
