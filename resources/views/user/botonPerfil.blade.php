<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 17/05/2018
 * Time: 19:11
 */
?>
@if(!isset($user))
    @if(!$friends->contains('id',$channel->user->id) && $channel->user->id != $logged_user->id)
        <button id="add_friend" class="btn_jugar" onclick="addFriend({{$channel->user->id}})" >Follow</button>
    @elseif($channel->user->id == $logged_user->id)
    @else
        <button id="unfriend" class="dejar_de_seguir" onclick="unFriend({{$channel->user->id}})" >Unfollow</button>
    @endif
@else
    @if(!$friends->contains('id',$user->id) && $user->id != $logged_user->id)
        <button id="add_friend" class="btn_jugar" onclick="addFriend({{$user->id}})" >Follow</button>
    @elseif($user->id == $logged_user->id)
    @else
        <button id="unfriend" class="dejar_de_seguir" onclick="unFriend({{$user->id}})" >Unfollow</button>
    @endif
@endif