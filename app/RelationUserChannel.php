<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelationUserChannel extends Model
{
    protected $table = 'relation_channels_users';
}
