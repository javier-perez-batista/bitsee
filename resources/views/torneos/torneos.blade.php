<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 22/04/2018
 * Time: 18:18
 */
?>
    @foreach($torneos as $torneo)
            <li id="li_{{$torneo->id}}" class="list-group-item torneos_contenido_selected">
                <div class="torneo_lista_info" style="width: 10rem">
                    <span>@if(strtotime($torneo->play_date) === strtotime(date("Y/m/d"))) <a href="/public/channel/{{rand(1,50)}}"><i style="color: red;margin-right: 1rem;" class="far fa-eye"></i></a> @else <i style="margin-right: 1rem;" class="far fa-eye"></i> @endif  {{date('d-m-Y H:i',strtotime($torneo->play_date))}}</span>
                </div>
                <div class="torneo_lista_info" style="width: 3rem;">
                    <img class="img-thumbnail-custom" src="{{asset('storage/images')}}/{{$torneo->juego->thumbnail}}" alt="">
                </div>
                <div class="torneo_lista_info" style="width: 13rem;">
                    <span>{{$torneo->name}}</span>
                </div>
                <div class="torneo_lista_info" style="width: 4rem">
                    <span>{{$torneo->actual_players}}/{{$torneo->max_players}}</span>
                </div>
                <div class="torneo_lista_info" style="width: auto;position:absolute;right: 0;top: 30%;">
                        <input type="hidden" name="torneo_id_unirse" value="{{$torneo->id}}">
                    @if($torneo->actual_players != $torneo->max_players)
                        @if(!$torneos_apuntados->contains('tournament_id', $torneo->id))
                            <div id="jugar_torneo" onclick="jugarTorneo({{$torneo->id}})" class="btn_jugar">Play</div>
                        @else
                            <div id="jugar_torneo" onclick="dejarTorneo({{$torneo->id}})" class="btn_dejar">Leave</div>
                        @endif
                    @elseif($torneos_apuntados->contains('tournament_id', $torneo->id))
                        <div id="jugar_torneo" onclick="dejarTorneo({{$torneo->id}})" class="btn_dejar">Leave</div>
                    @else
                        <div id="jugar_torneo" class="btn_completo">Full</div>
                    @endif
                </div>
            </li>
    @endforeach