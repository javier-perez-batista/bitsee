<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 19/04/2018
 * Time: 19:55
 */
?>
<script
        src="http://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>

@extends('layouts.master')

@section('content')
    <div class="row" style="width: 95%">
        <div class="col-lg-5">
            <div class="torneos_formulario_filtro" >
                <div class="busca_tu_juego"><div style="display: inline-block;">Search your game!</div><button style="display: inline-block; " onclick="hideJuegos()" class="btn_jugar clear_buton">Clear</button></div>
                <input id="input_juegos" class="busca_tu_juego_input" type="text" onfocus="showJuegos()" oninput="getJuegos()">
                <div class="contenido_juegos" id="contenido_juegos">
                    @include('juegos.juegosDiv')
                </div>
            </div>
        </div>
            <div class="col-md-7">
                <input type="hidden" id="plataforma_activa" value="0">
                <div id="pestanya_0" class="torneos_botones_secciones active" onclick="cambiarPestanyaTorneo(0)">
                    <span class="pestanya">All</span>
                </div>
                <div id="pestanya_1" class="torneos_botones_secciones" onclick="cambiarPestanyaTorneo(1)">
                    <span class="pestanya">PC</span>
                </div>
                <div id="pestanya_2" class="torneos_botones_secciones" onclick="cambiarPestanyaTorneo(2)">
                    <span class="pestanya">PS4</span>
                </div>
                <div  id="pestanya_3" class="torneos_botones_secciones" onclick="cambiarPestanyaTorneo(3)">
                    <span class="pestanya">XBox ONE</span>
                </div>
                <div class="torneo_lista_info" style="width: 100%">

                </div>
                <div class="index_juegos">
                    <ul class="list-group">
                        <li class="list-group-item torneos_contenido_selected" style="border: solid 2px #31789e;background: #fff;color: #31789e;border-radius: 2px;padding: 0rem 0.5rem;font-weight: bold;"> <div style="width: 27%;float: left; text-align: center"><span>Date/Time</span></div>
                            <div style="width: 8%;float: left; text-align: center"><span>Game</span></div>
                            <div style="width: 14%;float: left; text-align: center"><span>Tournament</span></div>
                            <div style="width: 27%;float: left; text-align: right"><span>Players</span></div>
                            <div style="width: 22%;float: left; text-align: right"><span>Play!</span></div></li>
                    </ul>
                    <ul class="list-group"  id="torneos_contenido_selected">

                        @include('torneos.torneos')
                    </ul>
                </div>
            </div>
    </div>

@endsection
<meta name="_token" content="{!! csrf_token() !!}" />
