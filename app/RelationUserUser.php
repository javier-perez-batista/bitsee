<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property  user_followed
 */
class RelationUserUser extends Model
{
    protected $table = "relation_users_users";
}
