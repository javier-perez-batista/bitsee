<!DOCTYPE html>
<html>
<head>
    @include('layouts.scripts')
    <title>Bitsee</title>
</head>
<body>

<div class="fixed_navbar">
    @include('layouts.navbar')
</div>
<div class="row cien" id="content">
    <div class="col-lg-2 menuSideBar">
        <div class="top_menuSideBar">
            <div class="span_canales"><span>Channels you follow</span></div>
        </div>
        <div class="bot_menuSideBar">
            <ul id="canales_que_sigo">
                @include('layouts.sideBarChannels')
            </ul>
        </div>
        <div class="top_menuSideBar">
            <div class="span_canales"><span>Friends</span></div>
        </div>
        <div class="bot_menuSideBar">
            <ul id="amigos_que_sigo">
                @include('layouts.sideBarFriends')
            </ul>

        </div>

    </div>

    <div class="col-lg-10" style="overflow: auto;height: 95vh;">
        @yield('videoStream')
        @yield('content')
        @yield('content2')
    </div>
</div>
</body>
</html>