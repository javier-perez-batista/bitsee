@if(sizeof($channels)<1)
        <span style="font-size: 80%;color: grey;">You don't follow any channel</span>
@endif
@foreach($channels as $channel)
        <li><a href="/public/channel/{{$channel->id}}">{{$channel->name}}</a></li>
@endforeach