<?php

use Faker\Generator as Faker;

$factory->define(App\Teams::class, function (Faker $faker) {
    return [
        'name' => $faker->userName,
        'logo' => $faker->image('public/storage/images/',140 ,140, null, false),
        'players' => "'".rand(1,50).",".rand(1,50).",".rand(1,50).",".rand(1,50).",".rand(1,50)."'",
        'captain' => rand(1,50)
    ];
});
