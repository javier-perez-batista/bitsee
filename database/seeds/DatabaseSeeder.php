<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(ChannelsTableSeeder::class);
         $this->call(JuegosTableSeeder::class);
         $this->call(TournamentsTableSeeder::class);
         $this->call(PlatformsTableSeeder::class);
         $this->call(VideosTableSeeder::class);
         $this->call(ChatsTableSeeder::class);
    }
}
