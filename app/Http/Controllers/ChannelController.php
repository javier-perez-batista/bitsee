<?php

namespace App\Http\Controllers;

use App\Jobs\followChannel;
use App\Juego;
use App\RelationUserChannel;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use App\Channel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChannelController extends Controller
{


    public function index (){
        $channel_id = 1;
        $relation = DB::select('SELECT COUNT(*) as counter,channel_id FROM `relation_channels_users` GROUP BY channel_id ORDER BY counter DESC LIMIT 1');
        if(sizeof($relation)>0)$channel_id = $relation[0]->channel_id;
        $channel = Channel::findOrFail($channel_id);
        $user_id = $channel->user_id;
        $user = User::findOrFail($user_id);
        $juego_id = $channel->game;
        $juego = Juego::findOrFail($juego_id);
        return view('layouts.index')->with(compact('channel','user','juego'));
    }

    public function showChannel($id){
        $channel = Channel::findOrFail($id);
        $juego_id = $channel->game;
        $juego = Juego::findOrFail($juego_id);
        $user_id = $channel->user_id;
        $user = User::findOrFail($user_id);
        return view('channel.show')->with(compact('channel','juego','user'));
    }

    public function search(){
        $filtro = request('search_navbar');
        $channels = DB::select("SELECT * FROM channels WHERE name LIKE '%$filtro%'");
        $channelsAll = collect($channels);
        $channelsAll = Channel::all();
        $juegos = DB::select("SELECT * FROM juegos WHERE name LIKE '%$filtro%'");
        $juegos = collect($juegos);
        $users = DB::select("SELECT * FROM users WHERE name LIKE '%$filtro%' OR username LIKE '%$filtro%'");
        $users = collect($users);
        return view('search_results')->with(compact('channelsAll','juegos','users'));
    }

    public function followChannel($id_channel){
        $user_id = Auth::id();
        $relation = new RelationUserChannel();
        $relation->user_id = Auth::id();
        $relation->channel_id = $id_channel;
        $relation->save();
        $channel_update = Channel::findOrFail($id_channel);
        $channel_update->subscribers = $channel_update->subscribers +1;
        $channel_update->save();
        $channel = Channel::findOrFail($id_channel);
        $channels = DB::select("SELECT * FROM channels WHERE id IN (SELECT channel_id FROM relation_channels_users WHERE user_id=$user_id)");
        $channels = collect($channels);
        $juego_id = $channel->game;
        $juego = Juego::findOrFail($juego_id);
        $user_id = $channel->user_id;
        $user = User::findOrFail($user_id);
        $html = view('channel.lateral')->with(compact('channel','channels','user','juego'))->render();
        return $html;
    }

    public function unfollowChannel($id_channel){
        $user_id = Auth::id();
        RelationUserChannel::where('channel_id','=',$id_channel)->where('user_id','=',Auth::id())->delete();
        $channel_update = Channel::findOrFail($id_channel);
        $channel_update->subscribers = $channel_update->subscribers -1;
        $channel_update->save();
        $channel = Channel::findOrFail($id_channel);
        $channels = DB::select("SELECT * FROM channels WHERE id IN (SELECT channel_id FROM relation_channels_users WHERE user_id=$user_id)");
        $channels = collect($channels);
        $juego_id = $channel->game;
        $juego = Juego::findOrFail($juego_id);
        $user_id = $channel->user_id;
        $user = User::findOrFail($user_id);
        $html = view('channel.lateral')->with(compact('channel','channels','user','juego'))->render();
        return $html;
    }

    public function reloadChannels(){
        $user_id = Auth::id();
        $channels = DB::select("SELECT * FROM channels WHERE id IN (SELECT channel_id FROM relation_channels_users WHERE user_id=$user_id)");
        $channels = collect($channels);
        $html = view('layouts.sideBarChannels')->with(compact('channels'))->render();
        return $html;
    }

    public function verCanal($id_channel){
        $channel = Channel::findOrFail($id_channel);
        $juego_id = $channel->game;
        $juego = Juego::findOrFail($juego_id);
        $user_id = $channel->user_id;
        $user = User::findOrFail($user_id);
        $videos = Video::all();
        return view('channel.ver')->with(compact('channel','juego','user','videos'));
    }
}
