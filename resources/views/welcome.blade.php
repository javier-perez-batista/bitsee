<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
    <title>Hello, world!</title>
</head>
<body>
<div class="fixed_header">
    <span div="navbar_name_logo">STREAM</span>
    <span div="navbar_section"> Juegos </span>
    <span div="navbar_section"> Explorar </span>
    <input type="text" class="navbar_search">
</div>

<div id="">
    @yield('videoStream')

</div>

<!-- Load the Twitch embed script -->

</body>

</html>
