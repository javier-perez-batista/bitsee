<?php

use Faker\Generator as Faker;

$factory->define(App\Channel::class, function (Faker $faker) {
    return [
        'url'=>$faker->userName,
        'name'=>$faker->name,
        'user_id'=>$faker->randomDigitNotNull,
        'thumbnail' => $faker->image('public/storage/images/',400,300, null, false), // secret
        'title' => 'El show de ',
        'game' => rand(1,50),
        'subscribers' => rand(1000,10000),
        'spectators' => rand(100,1000)
    ];
});
