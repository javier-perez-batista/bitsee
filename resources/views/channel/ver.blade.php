@extends('layouts.master')

@section('content')
    <div class="row profile_banner">
        <img src="{{asset('/storage/images/')}}/{{$user->banner_image}}" alt="">
        <div class="image_profile">
            <img src="{{asset('/storage/images/')}}/{{$channel->thumbnail}}" alt="">
        </div>
        <div style="position: absolute;z-index: 10;bottom: 17%;left: 8%;font-size: 150%;">{{$channel->name}}</div>
        <div class="boton_perfil" id="boton_juego">
            <div style="background-color:#dc3545;border: solid 2px #68141c;" class="btn_jugar">
                <a style="color: #fff;" href="/public/channel/{{$channel->id}}">Live</a>
            </div>
        </div>
    </div>
    <div class="row " style="height: 6%;">
        <div class="tabs_juegos">
            <div class="tab_canales active_pestanyas" id="tab_videos" >
                <span>Videos</span>
            </div>
        </div>
        <div class="col-lg-12" id="contenido_pestanyas_juego">
            @include('videoList')
        </div>
    </div>

@endsection
