function cambiarPestanyaTorneo(id_platform) {
    $('#plataforma_activa').val(id_platform);
    var request = $.get('torneos/'+id_platform); // make request
    var container = $('#torneos_contenido_selected');

    request.done(function(data) { // success
        $('#pestanya_'+0).removeClass('active');
        $('#pestanya_'+1).removeClass('active');
        $('#pestanya_'+2).removeClass('active');
        $('#pestanya_'+3).removeClass('active');
        $('#pestanya_'+id_platform).addClass('active');
        container.html(data);
    });
}

function jugarTorneo(id_torneo) {
    var request = $.get('torneos/jugar/'+id_torneo);
    $('#li_'+id_torneo).wrap('<p/>');
    var container = $('#li_'+id_torneo).parent();
    container.addClass('p_de_ayuda_ajax');
    request.done(function (data) {
        container.html(data);
        $('#li_'+id_torneo).unwrap();
    });
}

function dejarTorneo(id_torneo) {
    var request = $.get('torneos/dejar/'+id_torneo);
    $('#li_'+id_torneo).wrap('<p/>');
    var container = $('#li_'+id_torneo).parent();
    container.addClass('p_de_ayuda_ajax');
    request.done(function (data) {
        container.html(data);
        $('#li_'+id_torneo).unwrap();
    });
}

function getJuegos() {
    var input = $('#input_juegos').val();
    var parametros = {'input':input};
    request = $.get('torneos/juegos/input/'+input);
    var container = $('#contenido_juegos');
    request.done(function (data) {
        container.html(data);
        showJuegos();
        //container.css('display','block');
    });
}

function addJuegoFiltro(id) {
    var juego = $('#juego_'+id).html();
    var input = $('#input_juegos').val(juego);
    $('#contenido_juegos').html('');
    var container = $('#torneos_contenido_selected');
    var id_plataforma = $('#plataforma_activa').val();
    var parametros = {'id_juego':id,'id_plataforma':id_plataforma};
    var request = $.get('torneos/juego/'+id+'/plataforma/'+id_plataforma);
    request.done(function (data) {
        container.html(data);
    });
}

function showJuegos() {
    $('#contenido_juegos').removeClass("fadeout").addClass("fadein");
}

function hideJuegos() {
    $('#input_juegos').val('');
    getJuegos();
}