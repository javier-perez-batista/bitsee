<a href="/public/" class="navbar_name_logo navbar_section">BITSEE</a>

    <a class="navbar_section" href="/public/explorar">Explore</a>
    <a class="navbar_section" href="/public/torneos">Tournaments</a>
    <a class="navbar_section" href="/public/equipos">Teams</a>

<div class="navbar_input_search">
    <form id="search_form" method="POST" action="/public/">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" name="search_navbar" id="search_navbar" placeholder="search...">
            <button type="submit" class="search_navbar_button" hidden ></button>
        </div>
    </form>
</div>

<div id="nabvar_user" style="width: 9%;" onclick="showProfileOptions()" class="navbar_user navbar_section">
    @if($logged_user->profile_picture!='')<img src="{{asset('/storage/images/')}}/{{$logged_user->profile_picture}}" alt="">
    @else<img src="{{asset('/storage/images/default.png')}}" alt="">
    @endif

    <span>{{$logged_user->username}}</span>
    <i id="caret" class="fa fa-caret-down"></i>
</div>
<div id="profile_options" class="profile_options hidden" onmouseleave="hideProfileOptions()">
    <div>
        <a style="width: 100%;" class="navbar_section" href="/public/user/{{$logged_user->id}}"><span>Profile</span></a>
    </div>
    <div class="logOut" onclick="logOut()">
        <span class="navbar_section">Log-out</span>
    </div>
</div>
