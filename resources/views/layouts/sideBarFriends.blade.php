@if(sizeof($friends)<1)
    <span style="font-size: 80%;color: grey;">You have no friends</span>
@endif
@foreach($friends as $friend)
    <li><a href="/public/user/{{$friend->id}}">{{$friend->username}}</a></li>
@endforeach
