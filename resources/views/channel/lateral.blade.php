<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 16/05/2018
 * Time: 18:50
 */?>
<div>
    <div style="padding-bottom: .3rem;"><a class="streaming_footer" href="/public/channel/{{$channel->id}}">{{$channel->title}}{{$channel->name}}</a>
        @if($channels->contains('id',$channel->id))
            <button style="float: right" class="dejar_de_seguir" onclick="unfollowChannel({{$channel->id}})">Unsubscribe</button>
        @else
            <button style="float: right" class="btn_jugar" onclick="followChannel({{$channel->id}})">Subscribe</button>
        @endif
        <div style="float: right;margin-right: 1rem;background-color:#31789e " class="btn_jugar">
            <a style="background: #31789e;color: #fff;" href="/public/channel/ver/{{$channel->id}}">Go channel</a>
        </div>
    </div>
    <div><a class="streaming_footer" href="/public/user/{{$user->id}}">{{$user->username}}</a><span> playing </span><a class="streaming_footer"
                href="/public/juegos/{{$juego->id}}">{{$juego->name}}</a>
        <span style="float: right;">{{$channel->subscribers}} subscribers</span>
    </div>
</div>

